<?php

namespace Instagram;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    public function post()
    {
        return $this->belongsTo('Instagram\Post');
    }

    public function user()
    {
        return $this->belongsTo('Instagram\User');
    }
}
