<?php

namespace Instagram;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function comments() {
        return $this->hasMany('Instagram\Comment');
    }

    public function likes() {
    	return $this->hasMany('Instagram\Like');
    }

    public function user() {
        return $this->belongsTo('Instagram\User');
    }
}
