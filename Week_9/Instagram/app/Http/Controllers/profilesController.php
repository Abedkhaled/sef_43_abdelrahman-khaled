<?php

namespace Instagram\Http\Controllers;

use Illuminate\Http\Request;

use Instagram\Http\Requests;

use Instagram\Post;

class profilesController extends Controller
{
    public function getProfile($id) {
    	$posts=Post::where('user_id',$id)->orderBy('created_at','desc')->get();
    	return view('Profile', compact('posts'));
    }
}
