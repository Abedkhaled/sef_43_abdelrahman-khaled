<?php

namespace Instagram\Http\Controllers;

use Illuminate\Http\Request;

use Instagram\Http\Requests;

use Instagram\Post;

class newPostsController extends Controller
{
    public function upload() {
    	return view('upload');
    }

    public function newPost(Request $request) {
    	$this->validate($request, ['image' => 'required|image|mimes:jpeg,png,jpg',]);
	    $image = $request->file('image');
	    $input['imagename'] = time().'_'.$request->user()->id.'.'.$image->getClientOriginalExtension();
	    $destinationPath = public_path('imgUsers');
	    $image->move($destinationPath, $input['imagename']);
	    $newPost = new Post();
	    $newPost->url_path = $input['imagename'];
	    $newPost->status = $request->get('status');
	    $newPost->user_id = $request->user()->id;
	    $newPost->save();
	    return redirect('/timeline');
    }

    public function getTimeline() {
    	$posts = Post::orderBy('updated_at', 'desc')
                ->paginate(10);
    	return view('timeline', compact('posts'));
    }

}
