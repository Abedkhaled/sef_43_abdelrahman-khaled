<?php

namespace Instagram\Http\Controllers;

use Illuminate\Http\Request;

use Instagram\Http\Requests;

use Instagram\Comment;

class commentsController extends Controller
{
    public function addComment (Request $request){
    	$newComment = new Comment();
	    $newComment->comment = $request->get('comment');
	    $newComment->user_id = $request->user()->id;
	    $newComment->post_id = $request->get('post');
	    $newComment->save();
	    return $newComment->comment;
    }
}
