<?php

namespace Instagram\Http\Controllers;

use Illuminate\Http\Request;

use Instagram\Http\Requests;

use Instagram\Like;

class likesController extends Controller
{
    public function addLike (Request $request){
    	$newComment = new Like();
	    $newComment->user_id = $request->user()->id;
	    $newComment->post_id = $request->get('post');
	    $newComment->is_liked = 1;
	    $newComment->save();
	    return $newComment->comment;
    }

    public function relike(Request $request) {
    	$user_id=$request->user()->id;
    	$post_id=$request->get('post');
    	Like::where('user_id',$user_id)->where('post_id',$post_id)->update(['is_liked' => 1]);
    }

    public function unlike(Request $request) {
    	$user_id=$request->user()->id;
    	$post_id=$request->get('post');
    	Like::where('user_id',$user_id)->where('post_id',$post_id)->update(['is_liked' => 0]);
    }
}
