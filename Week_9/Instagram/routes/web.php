<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/','HomeController@getFirst');

Route::get('/timeline','newPostsController@getTimeline');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/upload','newPostsController@upload');

Route::post('/newpost','newPostsController@newPost');

Route::get('/profile/{id}','profilesController@getProfile');

Route::post('/addComment','commentsController@addComment');

Route::post('/addLike','likesController@addLike');

Route::post('/unLike','likesController@unlike');

Route::post('/reLike','likesController@relike');