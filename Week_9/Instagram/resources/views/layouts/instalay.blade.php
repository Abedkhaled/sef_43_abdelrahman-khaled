<!DOCTYPE html>

<html>
	<head>

	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<link href={{ url('/css/app.css') }} rel="stylesheet">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.0.0/bootstrap-social.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.0.0/bootstrap-social.min.css">
    <script>var token="{{ csrf_token() }}"; </script>


    <style>

    body {
        background-color: white;
    }

    #page-head {
        width:100%;
        height:60px;
        background-color: #125688;
    }

    #logo {
        height:60px;
        width:180px;
        float:left;
    }

    #logo img {
            height:60px;
            width:100%;
        }

    #a-h-l {
        width:200px;
        float:right;
    }

    #profile {
        height:60px;
        width:100px;
        float:left;
        border-right: 1px solid;
        border-left:1px solid;
        color:white;
        font-size:15px;
        text-align: center;
        padding-top: 15px;
    }

    #logout {
        height:60px;
        width:100px;
        float:left;
        border-right: 1px solid;
        border-left:1px solid;
        color:white;
        font-size: 15px;
        text-align: center;
        padding-top: 15px;
    }

    .center {
        margin-top: 50px;
    }

    .footer {
        width:100%;
    }
        
    </style>

	</head>

	<body>
        <div id="big-container" class="col-lg-12 centered">
            <div id="page-head">
                <div id="logo">
                    <a href="{{ url('/timeline') }}"><img src="{!! asset('icon.png') !!}"></a>
                </div>
                <div id="a-h-l">
                     @if (!Auth::guest())
                        <div id="profile"><a href = "{{ url('/profile/'.Auth::user()->id) }}">Visit profile</a></div>
                        <div id="logout"><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                            </form>
                        </div>
                     @else
                        <div  id="profile"><a href="{{ url('/login') }}" >Login</a></div>
                        <div id="logout"><a href="{{ url('/register') }}" class=>Register</a></div>
                     @endif
                </div>
            </div>
        		<div class="col-lg-8 col-lg-offset-2 centered center">
        		@yield('content')
        		</div>
            
            </div>
        </div>
        <script src="js/main.js"></script>
	</body>

</html>
