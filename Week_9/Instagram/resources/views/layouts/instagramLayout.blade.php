<!DOCTYPE html>

<html>
	<head>

	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<link href={{ url('/css/app.css') }} rel="stylesheet">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.0.0/bootstrap-social.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.0.0/bootstrap-social.min.css">
    <script>var token="{{ csrf_token() }}"; </script>


    <style>

        body {
            background-color: white;
        }

        .menu {
            margin-top: 50px;
            float:right;
            width:200px;
        }

        .page-head {
            width:100%;
            height:100px;
            background-color: blue;
        }

        .right-menu {
            max-width: 200px;
        }

        .center {
            width:100%;
            margin :0 auto;
            margin-top: 300px;
        }

        .logo {
            margin-top: 80px;
            float:left;
        }

        .logo img {
            height:60px;
            width:140px;
        }
    </style>

	</head>

	<body>
        <div class="col page-head">
        <a href="{{ url('/timeline') }}"><div class="logo"><img src="{!! asset('icon.png') !!}"></div></a>
        @if (!Auth::guest())
        <div class="menu">
            <a href = "{{ url('/upload') }}" class="btn btn-block btn-social btn-instagram right-menu">
                <span class="fa fa-instagram"></span> Add a new post
            </a>
            <a href = "{{ url('/profile') }}" class="btn btn-block btn-social btn-instagram right-menu">
                <span class="fa fa-instagram"></span> Visit profile
            </a>
            <a href="{{ url('/logout') }}" class="btn btn-block btn-social btn-instagram right-menu" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="fa fa-instagram"></span>
                Logout
            </a>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
            </form>
            @else 
            <div class="menu">
                <a href="{{ url('/login') }}" class="btn btn-block btn-social btn-instagram right-menu">
                <span class="fa fa-instagram"></span>Login</a>
                <a href="{{ url('/register') }}" class="btn btn-block btn-social btn-instagram right-menu">
                <span class="fa fa-instagram"></span>Register</a>
            </div>
        </div>

        @endif

		</div>
		<div class="center">
		@yield('content')
		</div>
        <script src="js/main.js"></script>
	</body>

</html>
