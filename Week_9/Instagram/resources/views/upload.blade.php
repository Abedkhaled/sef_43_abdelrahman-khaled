@extends('layouts.instalay')

@section('content')
<div class="col-lg-8 col-offset-2">
	 <form action="{{ url('/newpost') }}" method="POST" enctype="multipart/form-data">
	 	{{ csrf_field() }}
	 	<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
		<div class="form-group">
		    <label>Add image:</label>
		    <input class="form-control" id="email" type="file" name="image" accept="image/*">
		</div>
		<div class="form-group">
		    <label>Choose status:</label>
		    <input class="form-control" name="status">
		</div>
		<button type="submit" class="btn btn-default">Submit</button>
	</form>
</div>
@stop