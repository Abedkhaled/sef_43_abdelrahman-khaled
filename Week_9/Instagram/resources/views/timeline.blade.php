@extends('layouts.instalay')

<style>

.vertical-center{
  	width:80%;
  	margin: 0 auto;
}

.image {
	height:600px;
	width:100%;
}

.image img {
	height:600px;
	width:100%;
}

.like {
	display:block;
	height:50px;
	width:50px;
	background-size: 100% 100%;
	background-repeat: no-repeat;
	background-image: url({!! asset('im.png') !!});
}

.like_pressed {
	display:block;
	height:50px;
	width:50px;
	background-size: 100% 100%;
	background-repeat: no-repeat;
	background-image: url({!! asset('ima.png') !!});
}


.like button {
	width:100%;
	height:50px;
	background: transparent;
    border: none !important;
    font-size:0;
}

.like_pressed button {
	width:100%;
	height:50px;
	background: transparent;
    border: none !important;
    font-size:0;
}

.comments input {
	border:none;
	color:black;
	width:150px;
}

.wid {
	max-width:20px;
}

.user {
	color:#125688;
	font-size: 16px;
}

</style>

@section('content')

	<a href = "{{ url('/upload') }}" class="btn btn-block btn-social btn-instagram right-menu">
                <span class="fa fa-instagram"></span> Add a new post
    </a>
	@foreach ($posts as $post)
	<div class="vertical-center">
		<a href={{url ('/profile/'.$post->user->id)}}><div class="user">{{$post->user->name}}</div></a>
		<div class="image"><img src="{!! url('/imgUsers/'.$post->url_path) !!}"></div>
		<div class="status">{{ $post->user->name }}: {{ $post->status }}</div>
		@if (count($post->likes))
		@foreach ($post->likes as $like)
			@if ($like->user_id==Auth::user()->id && $like->is_liked==1)
				<div id="{{$post->id}}l" class ="like_pressed"><button onclick="likeBut(this.id,this.name)" id="{{$post->id}}" name="liked"></button></div>
			@else 
				<div id="{{$post->id}}l" class ="like"><button onclick="likeBut(this.id,this.name)" id="{{$post->id}}" name="unliked"></button></div>
			@endif
		@endforeach
		@else
		<div id="{{$post->id}}l" class ="like"><button onclick="likeBut(this.id,this.name)" id="{{$post->id}}" name="notPressed">like</button></div>
		@endif

		<div id="{{$post->id}}n" class="likers">{{count($post->likes->where('is_liked',1))}}</div>likes
		<div class="comments">
			<div id="{{$post->id}}c">
				@foreach ($post->comments as $comment)
				<div class="c">
				{{$comment->user->name}}: {{$comment->comment}}
				</div>
				@endforeach
			</div>
			<input type="text" id = "{{$post->id}}p" name="comment" class="comment-input" placeholder="..." >
			<button type="submit" name="{{Auth::user()->name}}" onclick="commentAjax(this.id,this.name)" id = "{{$post->id}}" class="btn btn-block btn-social btn-instagram ">Reply</button>
		</div>
	</div>
	@endforeach
@stop