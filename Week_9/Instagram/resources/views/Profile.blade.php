@extends('layouts.instalay')

<style>

#collection {
	width:100%;
}

.item {
	height:200px;
	width:33%;
	float:left;
	padding: 10px;
}

.item img {
	max-width:100%;
	max-height:100%;
}

</style>

@section('content')
<div id="prof">
	<h3></h3>
</div>
<div id="collection">
	@foreach ($posts as $post)
		<div class="item">
			<img src="{!! url('/imgUsers/'.$post->url_path) !!}">
		</div>
	@endforeach
</div>

@stop