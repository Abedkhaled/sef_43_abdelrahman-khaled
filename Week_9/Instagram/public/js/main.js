
function commentAjax(id,name)
{
    xmlhttp=new XMLHttpRequest();
    var comId=id+"p";
    var text=document.getElementById(comId).value;
    var data="post="+id+"& comment="+text+"& _token="+token;
    xmlhttp.open("POST","addComment",true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            var com=document.createElement("div");
            var status=name+": "+text;
            var comC=id+"c";
            com.innerHTML=status;
            document.getElementById(comC).appendChild(com);
        }
    }
    xmlhttp.send(data);
}

function likeAjax(id) {
    xmlhttp=new XMLHttpRequest();
    var data="post="+id+"& _token="+token;
    xmlhttp.open("POST","addLike",true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById(id).name="liked";
            document.getElementById(id).innerHTML="unlike";
            var divId=id+"l";
            var dId=id+"n";
            document.getElementById(divId).className="like_pressed";
            var number=document.getElementById(dId).innerHTML;
            number++;
            document.getElementById(dId).innerHTML=number;
        }
    }
    xmlhttp.send(data);
}

function unlikeAjax(id) {
    xmlhttp=new XMLHttpRequest();
    var data="post="+id+"& _token="+token;
    xmlhttp.open("POST","unLike",true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById(id).name="unliked";
            document.getElementById(id).innerHTML="like";
            var divId=id+"l";
            var dId=id+"n";
            document.getElementById(divId).className="like";
            var number=document.getElementById(dId).innerHTML;
            number--;
            document.getElementById(dId).innerHTML=number;
        }
    }
    xmlhttp.send(data);
}

function relikeAjax(id) {
    xmlhttp=new XMLHttpRequest();
    var data="post="+id+"& _token="+token;
    xmlhttp.open("POST","reLike",true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById(id).name="liked";
            document.getElementById(id).innerHTML="unlike";
            var divId=id+"l";
            var dId=id+"n";
            document.getElementById(divId).className="like_pressed";
            var number=document.getElementById(dId).innerHTML;
            number++;
            document.getElementById(dId).innerHTML=number;
        }
    }
    xmlhttp.send(data);
}

function likeBut(id, state) {
    if (state=="notPressed") {
        likeAjax(id);
   } else if (state=="liked") {
        unlikeAjax(id);
   } else {
    relikeAjax(id);
   }

}