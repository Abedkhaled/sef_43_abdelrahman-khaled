<?php

namespace PlaSta;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    public function plays() {
        return $this->belongsTo('PlaSta\League');
    }
}
