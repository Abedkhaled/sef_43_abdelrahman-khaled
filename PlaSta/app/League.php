<?php

namespace PlaSta;

use Illuminate\Database\Eloquent\Model;

class League extends Model
{
    public function has() {
    	return $this->hasMany('PlaSta\Player');
    }
}
