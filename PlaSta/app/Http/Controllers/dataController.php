<?php

namespace PlaSta\Http\Controllers;

use Illuminate\Http\Request;

use PlaSta\Http\Requests;

class dataController extends Controller
{
    function getTable($id) {
      $url = 'http://api.football-data.org/v1/competitions/'.$id.'/leagueTable';
    	//initialzing the curl
      $ch = curl_init($url);
      //set curl headers
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
      curl_setopt($ch, CURLOPT_HTTPGET, 1);
      //getting the curl response
      $response = curl_exec($ch);
      //returning response
      echo $response;
    }

    function getPlayer($league_id,$id){
      $url='https://api.fantasydata.net/soccer/v2/json/PlayerSeasonStatsByPlayer/'.$league_id.'/'.$id;
      //initialzing the curl
      $ch=curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json','Ocp-Apim-Subscription-Key: b9cb170d5b2c4b53af08f17320b9e888'));
      //setting it as a get
      curl_setopt($ch, CURLOPT_HTTPGET, 1);
      //executing the curl
      $response = curl_exec($ch);
      //returning response
      echo $response;
    }

    function getScores($id){
      //creating api url
      $url='https://api.crowdscores.com/api/v1/matches?competition_id='.$id.'&from=2016-10-14T12:00:00-03:00&to=2016-10-16T12:00:00-04:00';
      //initializing curl function with its headers
      $ch=curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json','x-crowdscores-api-key: 29344cf9287e4d1392bb57b4b0875c63'));
      //setting it as a get
      curl_setopt($ch, CURLOPT_HTTPGET, 1);
      //executing the curl
      $response = curl_exec($ch);
      //returning response
      echo $response;
    }
}
