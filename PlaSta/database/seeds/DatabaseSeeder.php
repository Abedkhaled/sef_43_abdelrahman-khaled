<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('leagues')->insert(
            array(
                    array(
                            'name' => 'premier league',
							'apiId' => 1,
                    ),
                    array(
                            
				            'name' => 'la liga',
				            'apiId' => 12,
                    ),
                    array(
                            
				            'name' => 'bundesliga',
				            'apiId' => 2,
                    ),
                    array(
                            
				            'name' => 'serie a',
				            'apiId' => 14,
                    ),
                    array(
                            
				            'name' => 'ligue 1',
				            'apiId' => 53,
                    ),
            ));

        DB::table('players')->insert(
            array(
                    array(
                            'name' => 'eden hazard',
                            'api_id' => '90026330',
							'league_apiId' => 1,
                    ),
                    array(
                            'name' => 'lionel messi',
                            'api_id' => '90027771',
							'league_apiId' => 12,
                    ),
                    array(
                            'name' => 'cristiano ronaldo',
                            'api_id' => '90029228',
							'league_apiId' => 12,
                    ),
                    array(
                            'name' => 'alexis sanchez',
                            'api_id' => '90026279',
							'league_apiId' => 1,
                    ),
                    array(
                            'name' => 'danny rose',
                            'api_id' => '90026729',
							'league_apiId' => 1,
                    ),
                    array(
                            'name' => 'kieran richardson',
                            'api_id' => '90026305',
							'league_apiId' => 1,
                    ),
                    array(
                            'name' => 'maxwell',
                            'api_id' => '90029075',
							'league_apiId' => 53,
                    ),
                    array(
                            'name' => 'carlos viguaray',
                            'api_id' => '90029594',
							'league_apiId' => 12,
                    ),
                    array(
                            'name' => 'lombain',
                            'api_id' => '90029631',
							'league_apiId' => 12,
                    ),
                    array(
                            'name' => 'juan iturbe',
                            'api_id' => '90026254',
							'league_apiId' => 14,
                    ),
                    array(
                            'name' => 'kevin de bruyne',
                            'api_id' => '90026330',
							'league_apiId' => 2,
                    ),
                    
        ));
    }
}
