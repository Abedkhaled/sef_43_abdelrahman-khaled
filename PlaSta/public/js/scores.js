function getScores (league) {
      //getting the select value
      var league_id=league.options[league.selectedIndex].value;
      document.getElementById('real_scores').innerHTML='';
      var load=document.createElement('div');
      load.className="load";
      document.getElementById('real_scores').appendChild(load);
      //creating an asynchronous xml http request
      xmlhttp=new XMLHttpRequest();
      xmlhttp.open("GET","getScores/"+league_id,true);
      xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xmlhttp.onreadystatechange=function()
      {
            if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
                  //emptying the output div
                  document.getElementById('real_scores').innerHTML='';
                  //parsing the json data
                  json=JSON.parse(this.responseText);
                  //creating the elements while looping through the data
                  for (scores in json) {
                        var score=document.createElement('div');
                        score.className="single_score";
                        score.innerHTML="<img src='"+json[scores].homeTeam.shirtUrl+"'>"+json[scores].homeTeam.shortName+" "+json[scores].homeGoals+" - "+json[scores].awayGoals+" "+json[scores].awayTeam.shortName+"<img src='"+json[scores].awayTeam.shirtUrl+"'>";
                        document.getElementById('real_scores').appendChild(score);
                  }
            }
      }

      xmlhttp.send();
}
