function getPlayer (input,div,key,league) {
    //getting the league id from the select tag
    var league_id=league.options[league.selectedIndex].value;
    //new http request
	xmlhttp=new XMLHttpRequest();
    //setting the get request with its parameters
	xmlhttp.open("GET","searchPlayer/"+key+"/"+league_id,true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.onreadystatechange=function() {
        //emptying the div
        div.innerHTML="";
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            //parsing the returning data
        	var suggestions=JSON.parse(this.responseText);
            //looping over the retuned data
        	for (players in suggestions) {
                //creating each div with its elements
        		var sugg=document.createElement("div");
        		sugg.className="sugg";
        		sugg.innerHTML=suggestions[players].name;
                sugg.value=suggestions[players].api_id;
        		div.appendChild(sugg);
                //onclick function when made it puts the selected in the input
        		sugg.onclick=function (){
        			input.value=this.innerHTML;
        			div.value=this.value;
        			div.innerHTML="";
        		}
        	}
        }
    }
    xmlhttp.send();
}
