function getTable(league) {
      xmlhttp=new XMLHttpRequest();
	xmlhttp.open("GET","getTable/"+league,true);
      xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xmlhttp.onreadystatechange=function()
      {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById('output').scrollIntoView();
            //emptying the div
            document.getElementById("output").innerHTML="";
            //parsing the data
        	var json = JSON.parse(this.responseText);
            //setting the columns header elements
            var leagueName=json.leagueCaption;
            var teams=json.standing;
            var thead=document.createElement("div");
            document.getElementById("output").appendChild(thead);
            var crestname=document.createElement("div");
            crestname.className="ele";
            crestname.innerHTML=json.leagueCaption;
            thead.appendChild(crestname);
            //creating a div and appending its data
            var name=document.createElement("div");
            name.className="ele";
            name.innerHTML="Name";
            thead.appendChild(name);
            //creating a div and appending its data
            var goals=document.createElement("div");
            goals.className="ele";
            goals.innerHTML="Goals";
            thead.appendChild(goals);
            //creating a div and appending its data
            var goals_against=document.createElement("div");
            goals_against.className="ele";
            goals_against.innerHTML="Goals against";
            thead.appendChild(goals_against);
            //creating a div and appending its data
            var goalDifference=document.createElement("div");
            goalDifference.className="ele";
            goalDifference.innerHTML="Goal difference";
            thead.appendChild(goalDifference);
            //creating a div and appending its data
            var wins=document.createElement("div");
            wins.className="ele";
            wins.innerHTML="Wins";
            thead.appendChild(wins);
            //creating a div and appending its data
            var draws=document.createElement("div");
            draws.className="ele";
            draws.innerHTML="Draws";
            thead.appendChild(draws);
            //creating a div and appending its data
            var losses=document.createElement("div");
            losses.className="ele";
            losses.innerHTML="Losses";
            thead.appendChild(losses);
            //creating a div and appending its data
            var played=document.createElement("div");
            played.className="ele";
            played.innerHTML="Matches";
            thead.appendChild(played);
            //creating a div and appending its data
            var points=document.createElement("div");
            points.className="ele";
            points.innerHTML="Points";
            thead.appendChild(points);

            //looping through teams
            for (teams in json.standing) {
            //setting the rows
            var badge=document.createElement("div");
            var myImage = new Image(40, 40);
            myImage.src = json.standing[teams].crestURI;
            badge.appendChild(myImage);
            badge.className="ele";
            thead.appendChild(badge);
            //creating a div and appending its data
            var tname=document.createElement("div");
            tname.className="ele";
            tname.innerHTML=json.standing[teams].teamName;
            thead.appendChild(tname);
            //creating a div and appending its data
            var goals=document.createElement("div");
            goals.className="ele";
            goals.innerHTML=json.standing[teams].goals;
            thead.appendChild(goals);
            //creating a div and appending its data
            var goals_against=document.createElement("div");
            goals_against.className="ele";
            goals_against.innerHTML=json.standing[teams].goalsAgainst;
            thead.appendChild(goals_against);
            //creating a div and appending its data
            var goalDifference=document.createElement("div");
            goalDifference.className="ele";
            goalDifference.innerHTML=json.standing[teams].goalDifference;
            thead.appendChild(goalDifference);
            //creating a div and appending its data
            var wins=document.createElement("div");
            wins.className="ele";
            wins.innerHTML=json.standing[teams].wins;
            thead.appendChild(wins);
            //creating a div and appending its data
            var draws=document.createElement("div");
            draws.className="ele";
            draws.innerHTML=json.standing[teams].draws;
            thead.appendChild(draws);
            //creating a div and appending its data
            var losses=document.createElement("div");
            losses.className="ele";
            losses.innerHTML=json.standing[teams].losses;
            thead.appendChild(losses);
            //creating a div and appending its data
            var played=document.createElement("div");
            played.className="ele";
            played.innerHTML=json.standing[teams].playedGames;
            thead.appendChild(played);
            //creating a div and appending its data
            var points=document.createElement("div");
            points.className="ele";
            points.innerHTML=json.standing[teams].points;
            thead.appendChild(points);
            }
        }
    }
    xmlhttp.send();
}


