function getPlayerStats(kit,league,id,output) {
	var league_id=league.options[league.selectedIndex].value;
	xmlhttp=new XMLHttpRequest();
	xmlhttp.open("GET","getPlayer/"+league_id+"/"+id,true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
        	//emptying the container
        	output.innerHTML="";
        	//parsing the response data string into objects
        	var json=JSON.parse(this.responseText);
        	//create div and append its data
            var logo=document.createElement('div');
            var logoDiv=document.createElement('div');
            if (kit=="lionel messi") {
                logoDiv.className="m elem img-circle";
            }
            else if (kit=="cristiano ronaldo") {
                logoDiv.className="c elem img-circle";
            }
            else {
                logoDiv.className="a elem img-circle";
            }
            logo.appendChild(logoDiv);
            output.appendChild(logo);
        	var name=document.createElement('div');
        	name.className=output.className+" cred";
        	name.innerHTML="<p class='"+output.className+"-data'>"+json[0].Name+"</p><br/> "+json[0].Team;
        	output.appendChild(name);
        	//create div and append its data
        	var games = document.createElement('div');
        	games.className=output.classname+" part";
        	games.innerHTML="Games played : <p class='"+output.className+"-data'>"+json[0].Games+"</p>";
        	output.appendChild(games);
        	var goals = document.createElement('div');
        	goals.className=output.classname+" part";
        	goals.innerHTML="Goals : <p class='"+output.className+"-data'>"+json[0].Goals+"</p>";
        	output.appendChild(goals);
        	//create div and append its data
        	var assists = document.createElement('div');
        	assists.className=output.classname+" part";
        	assists.innerHTML="Assists : <p class='"+output.className+"-data'>"+json[0].Assists+"</p>";
        	output.appendChild(assists);
        	//create div and append its data
        	var passes = document.createElement('div');
        	passes.className=output.classname+" part";
        	passes.innerHTML="Passes : <p class='"+output.className+"-data'>"+json[0].Passes+"</p>";
        	output.appendChild(passes);
        	//create div and append its data
        	var shots = document.createElement('div');
        	shots.className=output.classname+" part";
        	shots.innerHTML="Shots : <p class='"+output.className+"-data'>"+json[0].Shots+"</p>";
        	output.appendChild(shots);
        	//create div and append its data
        	var tackles = document.createElement('div');
        	tackles.className=output.classname+" part";
        	tackles.innerHTML="Tackles : <p class='"+output.className+"-data'>"+json[0].TacklesWon+"</p>";
        	output.appendChild(tackles);
        	//create div and append its data
        	var fant = document.createElement('div');
        	fant.className=output.classname+" part";
        	fant.innerHTML="Fantasy points : <p class='"+output.className+"-data'>"+json[0].FantasyPoints+"</p>";
        	output.appendChild(fant);
        }
    }
    xmlhttp.send();
}