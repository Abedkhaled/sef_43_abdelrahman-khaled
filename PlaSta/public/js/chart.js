function getChart1 (left,right){
	//loading the chart library
	google.charts.load('current', {'packages':['bar','corechart']});
    google.charts.setOnLoadCallback(drawChart1);
    
    //function create chart
    function drawChart1() {

        var data = google.visualization.arrayToDataTable([
        	//setting the chart data

	        ['Performance', left[0].innerHTML, right[0].innerHTML],
	        ['Games Played', parseInt(left[1].innerHTML), parseInt(right[1].innerHTML)],
	        ['Goals', parseInt(left[2].innerHTML), parseInt(right[2].innerHTML)],
	        ['Assists', parseInt(left[3].innerHTML), parseInt(right[3].innerHTML)],
	        ['Passes/game', parseInt(left[4].innerHTML)/parseInt(left[1].innerHTML), parseInt(right[4].innerHTML)/parseInt(right[1].innerHTML)],
	        ['Shots/game',parseInt(left[5].innerHTML)/parseInt(left[1].innerHTML), parseInt(right[5].innerHTML)/parseInt(right[1].innerHTML)],
	        ['Tackles',parseInt(left[6].innerHTML), parseInt(right[6].innerHTML)],
	        ['Fantasy points',parseInt(left[7].innerHTML)/parseInt(left[1].innerHTML), parseInt(right[7].innerHTML)/parseInt(right[1].innerHTML)]
        ]);
        //chart legend
        var options = {
          chart: {
            title: 'League performance',
          }
        };
        //creating the chart
        chart = new google.charts.Bar(document.getElementById('chart'));
        //drawing the chart
        chart.draw(data, options);
    }
}	 

function getChart2 (left,right) {
	google.charts.setOnLoadCallback(drawChart2);
	function drawChart2() {
    if (document.getElementById('chart-head')!=null) {
      document.getElementById('chart-head').parentNode.removeChild(document.getElementById('chart-head'));
    }
		var head=document.createElement('h1');
    head.id="chart-head";
		head.innerHTML="Stat by stat comparison";
		document.getElementById('pies').insertBefore(head,document.getElementById('pies').firstChild);
    var data1 = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      [left[0].innerHTML,     parseInt(left[2].innerHTML)],
      [right[0].innerHTML,      parseInt(right[2].innerHTML)]
    ]);
    var data2 = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      [left[0].innerHTML,     parseInt(left[3].innerHTML)],
      [right[0].innerHTML,      parseInt(right[3].innerHTML)]
    ]);
    var data3 = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      [left[0].innerHTML,     parseInt(left[4].innerHTML)],
      [right[0].innerHTML,      parseInt(right[4].innerHTML)]
    ]);
    var data4 = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      [left[0].innerHTML,     parseInt(left[5].innerHTML)],
      [right[0].innerHTML,      parseInt(right[5].innerHTML)]
    ]);
    var data5 = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      [left[0].innerHTML,     parseInt(left[6].innerHTML)],
      [right[0].innerHTML,      parseInt(right[6].innerHTML)]
    ]);
    var data6 = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      [left[0].innerHTML,     parseInt(left[7].innerHTML)],
      [right[0].innerHTML,      parseInt(right[7].innerHTML)]
    ]);

    var options1 = {
      title: 'Goals'
    };
    var options2 = {
      title: 'Assists'
    };
    var options3 = {
      title: 'Passes per game'
    };
    var options4 = {
      title: 'Shots per game'
    };
    var options5 = {
      title: 'tackles'
    };
    var options6 = {
      title: 'Fantasy points'
    };
    var chart1 = new google.visualization.PieChart(document.getElementById('pie1'));
    var chart2 = new google.visualization.PieChart(document.getElementById('pie2'));
    var chart3 = new google.visualization.PieChart(document.getElementById('pie3'));
    var chart4 = new google.visualization.PieChart(document.getElementById('pie4'));
    var chart5 = new google.visualization.PieChart(document.getElementById('pie5'));
    var chart6 = new google.visualization.PieChart(document.getElementById('pie6'));
    chart1.draw(data1, options1);
    chart2.draw(data2, options2);
    chart3.draw(data3, options3);
    chart4.draw(data4, options4);
    chart5.draw(data5, options5);
    chart6.draw(data6, options6);
  }
}

//function that checks if the input data exists
function checkIfCompExists () {
	//checking wether both stats are initialized
	var left_data=document.getElementsByClassName("loutput-data");
    var right_data=document.getElementsByClassName("routput-data");
	if (left_data.length > 0  && right_data.length > 0) {
		//calling the draw chart function
		getChart1(left_data,right_data);
		getChart2(left_data,right_data);
    document.getElementById('chart').scrollIntoView();
	} else {
		//alerting that the data is not there
		alert ("No comparison has been made");
	}
}