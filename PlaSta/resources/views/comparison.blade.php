@extends('layouts.main-layout')

@section('content')

<script>
function clear (){
	document.getElementById("linput").value="";
	document.getElementById("rinput").value="";
}
window.onload=clear;
</script>
<br>
<div id="top"><h1> Choose players </h1></div>
<div id="comp">
	<div id="left">
		 <select id="lpane" class="form-control">
			<option value="1">Premier league</option>
			<option value="14">Serie a</option>
			<option value="12">Liga</option>
			<option value="53">Ligue 1</option>
			<option value="2">Bundesliga</option>
		</select>
		<br>
		<input type="text" id="linput" onkeyup="getPlayer(this,document.getElementById('left-search'),this.value,document.getElementById('lpane'))" class="form-control"><br>
		<div id="left-search"></div>
		<button class="but form-control" onclick="getPlayerStats(document.getElementById('linput').value,document.getElementById('lpane'),document.getElementById('left-search').value,document.getElementById('left-output'))">go</button>
		<div id="left-output" class="loutput"></div>
	</div>
	<div id="right">
		<select id="rpane" class="form-control">
			<option value="1">Premier league</option>
			<option value="14">Serie a</option>
			<option value="12">Liga</option>
			<option value="53">Ligue 1</option>
			<option value="2">Bundesliga</option>
		</select> 
		<br>
		<input type="text" id="rinput" onkeyup="getPlayer(this,document.getElementById('right-search'),this.value,document.getElementById('rpane'))" class="form-control"><br>
		<div id="right-search"></div>
		<button class="but form-control" onclick="getPlayerStats(document.getElementById('rinput').value,document.getElementById('rpane'),document.getElementById('right-search').value,document.getElementById('right-output'))">go</button>
		<div id="right-output" class="routput"></div>
	</div>
	<div id="chartIt"><button id="chart-but" onclick="checkIfCompExists()">See it on charts</button></div>
	<div id="chart">
	</div>
	<div id="pies">
		<div id="pie1" class="pie">
		</div>
		<div id="pie2" class="pie">
		</div>
		<div id="pie3" class="pie">
		</div>
		<div id="pie4" class="pie">
		</div>
		<div id="pie5" class="pie">
		</div>
		<div id="pie6" class="pie">
		</div>
	</div>
	<div id="footer">
        <div id="credits">Copyright of abed<br>All right reserved</div>
    </div>
</div>
@endsection 