@extends('layouts.main-layout')

@section('content')
<div id="first-page">
	<div id="top-main">
		<div id="image" class="col-lg-7 span10">
			<a href="{{ url('/comp') }}" class="butt">Stats Center</a>
		</div>
		<div id="scores" class="col-lg-4 col-lg-offset-1">
			<div id="scores_head">
			<select id="score_pick" class="form-control" onchange="getScores(this)">
				<option value="2">Premier league</option>
				<option value="49">Serie a</option>
				<option value="46">Liga</option>
				<option value="47">Ligue 1</option>
				<option value="48">Bundesliga</option>
			</select>
			</div>
			<div id="real_scores">
			</div>
		</div>
	</div>
	<div id="articles">
		<div id="topline"></div>
	</div>
	<div id="middle">
		<div id="map" class="col-lg-7">
			<div class="flag eng"><a onclick="getTable('398')"><img class="img-responsive" src="{!! asset('England.png') !!}"></a></div>
			<div class="flag fra"><a onclick="getTable('396')"><img class="img-responsive" src="{!! asset('France.png') !!}"></a></div>
			<div class="flag ita"><a onclick="getTable('401')"><img class="img-responsive" src="{!! asset('Italy.png') !!}"></a></div>
			<div class="flag spa"><a onclick="getTable('399')"><img class="img-responsive" src="{!! asset('Spain.png') !!}"></a></div>
			<div class="flag ger"><a onclick="getTable('394')"><img class="img-responsive" src="{!! asset('germany.png') !!}"></a></div>
			<div class="flag por"><a onclick="getTable('402')"><img class="img-responsive" src="{!! asset('portugal.png') !!}"></a></div>
			<div class="flag ned"><a onclick="getTable('404')"><img class="img-responsive" src="{!! asset('netherland.png') !!}"></a></div>
		</div>
		<div id="news" class="col-lg-4 col-lg-offset-1">
			<div id="mvp"><h3>Player of the week</h3></div>
		</div>
	</div>
	<div id="output"></div>
	<div id="footer">
        <div id="credits">Copyright of abed<br>All right reserved</div>
    </div>

    <script>
    	window.onload=function(){
    		getTable('398');
    		getScores(document.getElementById('score_pick'));
    	}
    </script>

</div>
@endsection