<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>PlaSta</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link href={{ url('/css/app.css') }} rel="stylesheet">
    <link href={{ url('/css/main.css') }} rel="stylesheet">

     <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.0.0/bootstrap-social.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.0.0/bootstrap-social.min.css">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>
<body>
    <div id="container" class="col-lg-12">
        <div id="top-bar" class="col-lg-12">
            <div id="left-pane" class="col-lg-3 col-lg-offset-2">
                <a href="{{ url('/') }}"><img src="{!! asset('logo.png') !!}"></a>
            </div>
            <div id="middle-pane" class="col-lg-3">
                <a href="{{ url('/') }}"><h1>PlaSta</h1></a>
            </div>
        </div>        
    
        <div id ="main" class="col-lg-8 col-lg-offset-2 container">                  
            @yield('content')
        </div>
        
    </div>
    <script src="js/leagues.js"></script>
    <script src="js/players.js"></script>
    <script src="js/stats.js"></script>
    <script src="js/chart.js"></script>
    <script src="js/scores.js"></script>
</body>
</html>