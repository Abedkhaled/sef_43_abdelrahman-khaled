<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('main');
});

Route::get('/comp', 'pagesController@getComp');
Route::get('/searchPlayer/{key}/{league_id}','searchController@searchPlayer');

Route::get('/getTable/{id}','dataController@getTable');
Route::get('/getPlayer/{league_id}/{id}','dataController@getPlayer');
Route::get('/getScores/{id}','dataController@getScores');

Auth::routes();

Route::get('/home', 'HomeController@index');
