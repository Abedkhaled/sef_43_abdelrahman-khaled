<?php
class database{

	private $fileName;
	private $numberOfColumns;
	private $tableName;


	//set the dtabase name
	public function setFileName($databaseName){
		$this->fileName=$databaseName;
	}
	
	//get filename
	public function getFileName(){
		return $this->fileName;
	}

	//function that creates a folder to store the database
	public function createDatabase(){
		//$databaseFile=fopen($this->fileName,'w');
		mkdir("$this->fileName");
		echo $this->fileName." CREATED\n";
	}



	//function that deletes the database file
	public function deleteDatabase(){
		$filesInDirectory=scandir($this->fileName);
		foreach($filesInDirectory as $file){
			if($file!="." && $file!=".."){
				unlink($this->fileName."/".$file);
			}
		}
		rmdir($this->fileName);
		echo $this->fileName." DELETED\n";
	}



	//function that creates a table as a csv file
	public function createTable($tName,$columns){
		$this->tableName=$this->fileName.("/").$tName;
		$fileOfTable=fopen($this->tableName,'w');
		fputcsv($fileOfTable,$columns,",");
		$this->numberOfColumns=count($columns);
		echo $tName." CREATED\n";
	}




	//function that adds a record
	public function addRecord($newRecord){
		if (count($newRecord)==$this->numberOfColumns && is_numeric($newRecord[0])){
				$searchFile=fopen($this->tableName,'r');
				$data=array();
				while(!feof($searchFile)){
					array_push($data, fgetcsv($searchFile)[0]);
				}
				foreach($data as $firstElement){
					if($firstElement==$newRecord[0]){
						echo "Id already used\n";
						return;
					}
				}
				$insertFile=fopen($this->tableName,'a');
				fputcsv($insertFile,$newRecord,",");
				echo "Record ADDED\n";
		}
		else {
			echo "error--not valid entry\n";
		}
	}




	//function that retrieve a record
	public function retrieve($query){
		$searchFile=fopen($this->tableName,'r');
		$allData=array(array());
		while(!feof($searchFile)){
			array_push($allData, fgetcsv($searchFile));
		}
		fclose($searchFile);
		array_shift($allData);
		$array_count=count($allData);
		if (empty($allData[$array_count-1])){
			array_pop($allData);
		}
		foreach($allData as $data){
			foreach($data as $dataElement){
				if ($dataElement==$query){
					echo '"';
					echo join('","',$data);
					echo '"';
					echo "\n";
				}
			}
		}
	}






	//function that deletes an entry
	public function deleteRecord($query){
		if(is_numeric($query)){
			$i=0;
			$query=str_replace('"', "", $query);
			$searchFile=fopen($this->tableName,'r');
			$allData=array(array());
			while(!feof($searchFile)){
				array_push($allData, fgetcsv($searchFile));
			}
			fclose($searchFile);
			array_shift($allData);
			array_pop($allData);
			foreach($allData as $key=>$data){
				if($data[0]==$query){
					unset($allData[$key]);
					echo "Record deleted\n";	
				}
			}
			$deleteFile=fopen($this->tableName,'w+');
			foreach($allData as $data){
				fputcsv($deleteFile,$data,",");
			}
			fclose($deleteFile);
		}
		else {
			echo "Not an Id number\n";
		}
	}



}


?>