<?php
require_once("databaseClass.php");
$dBase=new database();
while(true){
	$input=readline();
	$stringArray=explode(',',$input);
	if($stringArray[0]=="CREATE"){
		//create database method
		if($stringArray[1]=="DATABASE"){
			$dBaseName=str_replace('"',"",$stringArray[2]);
			$dBase->setFileName($dBaseName);
			$dBase->createDatabase();
		}
		//create table method
		elseif($stringArray[1]=="TABLE"){
			$tableName=str_replace('"',"",$stringArray[2]);
			$columns=array();
			for($i=4;$i<count($stringArray);$i++){
				$stringArray[$i]=str_replace('"',"",$stringArray[$i]);
				array_push($columns,$stringArray[$i]);
			}
			$dBase->createTable($tableName,$columns);
		}
		else{
			echo "Invalid entry\n";
		}
	}
	//Delete method
	elseif($stringArray[0]=="DELETE"){
		if($stringArray[1]=="DATABASE"){
			$dBaseName=str_replace('"',"", $stringArray[2]);
			$dBase->setFileName($dBaseName);
			$dBase->deleteDatabase();
		}
		elseif($stringArray[1]=="ROW"){
			$query=str_replace('"',"",$stringArray[2]);
			$dBase->deleteRecord($query);
		}
	}
	//add method
	elseif ($stringArray[0]=="ADD"){
		$entries=array();
		for($i=1;$i<count($stringArray);$i++){
			$stringArray[$i]=str_replace('"',"",$stringArray[$i]);
			array_push($entries,$stringArray[$i]);
		}
		$dBase->addRecord($entries);
	}
	//get method
	elseif ($stringArray[0]=="GET"){
		$query=str_replace('"',"",$stringArray[1]);
		$dBase->retrieve($query);
	}
}


?>