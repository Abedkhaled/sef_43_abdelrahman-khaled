<?php

namespace blogApp\Http\Controllers;

use Illuminate\Http\Request;

use blogApp\Http\Requests;

use blogApp\Blog;

class BlogsController extends Controller
{
    public function getAllPosts() {
    	$blogs=Blog::orderBy('updated_at', 'desc')
                ->paginate(5);
    	return view('in',compact('blogs'));
    }

    public function getSinglePost($id) {
    	$post=Blog::where('id',$id)->first();
    	return view('singleBlog',compact('post'));
    }

    public function deletePost($id) {
    	$post=Blog::find($id);
    	$post->delete();
    	return redirect('/');
    }

    public function getNewView(){
    	return view('addPost');
    }

    public function submitNewPost(Request $request) {
    	$post = new Blog() ;
		$post->title = $request->get('title');
		$post->content = $request->get('content');
		$post->userId = $request->user()->id;
		$post->save();
		return redirect('/');
    }
}
