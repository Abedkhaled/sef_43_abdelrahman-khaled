@extends('layouts.app')

<style>

	#title {
		text-align: center;
		width:80%;
	}

	#blog-container {
		width:80%;
		margin:0 auto;
		padding:10px;
	}

	.single-blog {

		width:80%;
		margin:0 auto;
		border:1px solid;
		padding: 5px;
		margin-top: 20px;
	}

</style>

@section('content')

	<div id="title">
		<h1>A creative blogging name</h1>
	</div>
	<div id="blog-container">
	@foreach ($blogs as $blog)
		<div class="single-blog">
			<h3><a href="{{url('singleBlog/'.$blog->id)}}">{{ $blog->title }}</a></h3>
			<p>{{ str_limit($blog->content) }}</p>
		</div>
		@endforeach
	</div>
	
@stop