@extends('layouts.app')

<style>

#head {
	text-align: center;
}

#form{
	width:80%;
	margin:0 auto;
}

button {
	height:70px;
	width:97.5%;
	background-color:#7FFF00;
}

</style>

@section('content')
<div id="head"><h2>Blog whatever's on your mind!!!</h2></div>
<div id="form">
	<form action="{{ url('/submitPost') }}" method="POST">
		{{ csrf_field() }}
		<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
		Title:<br><input type="text" size="101" name="title"><br>
		Content:<textarea cols="100" rows="20" name="content"></textarea>
		<button type="submit">
        	Blog it
        </button>
	</form>

</div>

@stop