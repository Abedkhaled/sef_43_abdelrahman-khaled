@extends('layouts.app')

<style>

#title{
	width:80%;
	margin:0 auto;
	text-align: center;
} 

.text{
	width:80%;
	margin:0 auto;
	margin-top: 50px;
}

#delete-edit {
	width:80%;
	margin:0 auto;
}

</style>


@section('content')
<div id="title">
	<h1>{{$post->title}}</h1>
</div>

<div class="text">
	<p>{{$post->content}}</p>

</div>
@if(Auth::id() == $post->userId)
<div id="delete-edit">
	<a href="{{ url('deletePost/'.$post->id) }}">delete post</a>
</div>
@endif
@stop