<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'BlogsController@getAllPosts');

Route::get('/singleBlog/{id}','BlogsController@getSinglePost');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/deletePost/{id}', 'BlogsController@deletePost');

Route::get('/editPost/{id}', 'BlogsController@editPost');

Route::get('/addPost','BlogsController@getNewView');

Route::post('/submitPost','BlogsController@submitNewPost');




