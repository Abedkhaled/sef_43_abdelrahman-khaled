<?php
class GameOutput {
	private $numbersExpression=array();
	private $solution;
	private $target;
	function __Construct($array,$int,$Target){
		$this->numbersExpression=$array;
		$this->solution=$int;
		$this->target=$Target;
	}


	//function that prints the solution
	function printSolution (){
		print("Target: ".$this->target."\n");
		if ($this->solution==0){
			echo "Solution [Exact]:\n";
			foreach ($this->numbersExpression as $term){
				echo $term." ";
			}
			echo "= ".$this->target;
		}
		else {
			print("Solution [Remaining: ".$this->solution."]:\n");
			foreach ($this->numbersExpression as $term){
				echo $term.(" ");
			}
			echo "= ".($this->target-$this->solution).("\n");
		}
	}
}


?>