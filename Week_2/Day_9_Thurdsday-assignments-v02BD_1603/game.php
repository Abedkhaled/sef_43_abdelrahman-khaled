<?php
//including the game generator, Game solver and Game output class files
require_once('/home/abed/SEF_43_Abdelrahman-Khaled/Week_2/Day_9_Thurdsday-assignments-v02BD_1603/GameGenerator.php');
require_once('/home/abed/SEF_43_Abdelrahman-Khaled/Week_2/Day_9_Thurdsday-assignments-v02BD_1603/GameSolver.php');
require_once('/home/abed/SEF_43_Abdelrahman-Khaled/Week_2/Day_9_Thurdsday-assignments-v02BD_1603/GameOutput.php');


//user input
print("How many games would you like me to play today?\n");
$games=intval(fgets(STDIN));


for ($i=0;$i<$games;$i++){
	//game generator object
	$game=new GameGenerator($i+1);

	//generates the number of counts in first set
	$game->generateDigitsCount();

	//randomize the input
	$game->randomDigitArray();

	//print input
	$game->printInput();

	//game solver object
	$solve=new GameSolver($game->getArray(),$game->getTargetNumber());

	//get combination
	$solve->array_combination();

	//get permutations
	$solve->permute_combinations();

	//generates all the possibilities
	$solve->getFixArray();

	//game output object
	$output=new GameOutput($solve->getGoalString(),$solve->getGoal(),$solve->getTargetNumber());

	//print output
	$output->printSolution();
	echo "\n---------\n";
}
?>