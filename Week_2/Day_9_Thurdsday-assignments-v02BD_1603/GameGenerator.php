<?php
class GameGenerator {
	private $digitarray=array(25,50,75,100);
	private $numberarray=array(1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10);
	private $numberArray=array();
	private $digitscount=0;
	private $targetNumber;
	private $numberOfGames;


	//constructor
	function __Construct($n){
		$this->numberOfGames=$n;
	}
	
	//get the number of elements from first array
	public function generateDigitsCount(){
		$this->digitscount=rand(1,4);
	}

	//returns the number of picks from the first array
	public function getDigitCount(){
		return $this->digitscount;
	}

	//get target number
	public function getTargetNumber(){
		return rand(101,999);
	}

	//print input
	public function printInput(){
		print("Game ".$this->numberOfGames.":\n");
		print ("{ ");
		foreach ($this->numberArray as $num){
			echo $num.(", ");
		}
		print("}\n\n");
	}

	//push the random values into the number array 
	public function randomDigitArray(){
		$arrvalue=array_rand($this->digitarray,$this->digitscount);
		$arrvalue1=array_rand($this->numberarray,6-$this->digitscount);
		if (is_array($arrvalue)){
			foreach ($arrvalue as $value){
				array_push($this->numberArray,$this->digitarray[$value]);
			}
		}
		else {
			array_push($this->numberArray,$this->digitarray[$arrvalue]);
		}
		foreach ($arrvalue1 as $value){
			array_push($this->numberArray,$this->numberarray[$value]);
		}
	}
	public function getArray(){
		return $this->numberArray;
	}
}
?>
