<?php
class GameSolver {
	private $numberArray=array();
	private $combinatedNumberArray=array(array());
	private $target;
	private $goal=100;
	private $goalString;
	private $operators=array("+","-","*","/");
	private $stack=array(array());
	private $perms=array();

	//constructor
	function __Construct($array,$int){
		$this->numberArray=$array;
		$this->target=$int;
	}


	//get the initial array
	public function getArray(){
		return $this->numberArray;
	}

	//get target
	public function getTargetNumber(){
		return $this->target;
	}


	//function that returns the winning expression
	public function getGoalString(){
		return $this->goalString;
	}

	//function that returns the closest number
	public function getGoal(){
		return $this->goal;
	}

	//function that gets all the array combinations
	public function array_combination(){
		$this->combinatedNumberArray = array(array( ));

    	foreach ($this->numberArray as $element)
        	foreach ($this->combinatedNumberArray as $combination)
            	array_push($this->combinatedNumberArray, array_merge(array($element), $combination));
	}

	//function that will permute combinations
	public function permute_combinations(){
		$perms=array(array());
		foreach ($this->combinatedNumberArray as $combs){
			self::pc_permute($combs,$this->perms);
		}
	}

	//function that gets permutations of a specific string
	private function pc_permute ($items,$perms=array()){
		if (empty($items)) {
			array_pop($perms);
			array_push($this->combinatedNumberArray,$perms);
    	}else {
        	for ($i=count($items)-1;$i>=0;--$i) {
            	$newitems = $items;
            	$newperms = $perms;
            	list($foo) = array_splice($newitems, $i, 1);
            	array_unshift($newperms, $foo);
            	self::pc_permute($newitems, $newperms);
        	}
    	}
	}


	//function that will fill the array with the appropriate values
	public function getFixArray (){
		$temp=array();
		$i=0;
		foreach ($this->combinatedNumberArray as $number){
			if (count($number)==2){
				$combs=self::combos($this->operators,1);
				foreach($combs as $comb){
					$this->stack[$i][0]=$number[0];
					$this->stack[$i][1]=$comb[0];
					$this->stack[$i][2]=$number[1];
					$temp=$this->stack[$i];
					self::compare($temp);
					$i++;
				}
			}
			elseif (count($number)==3){
				$combs=self::combos($this->operators,2);
				foreach($combs as $comb){
					$this->stack[$i][0]=$number[0];
					$this->stack[$i][1]=$comb[0];
					$this->stack[$i][2]=$number[1];
					$this->stack[$i][3]=$comb[1];
					$this->stack[$i][4]=$number[2];
					$temp=$this->stack[$i];
					self::compare($temp);
					$i++;
				}
			}
			elseif (count($number)==4){
				$combs=self::combos($this->operators,3);
				foreach($combs as $comb){
					$this->stack[$i][0]=$number[0];
					$this->stack[$i][1]=$comb[0];
					$this->stack[$i][2]=$number[1];
					$this->stack[$i][3]=$comb[1];
					$this->stack[$i][4]=$number[2];
					$this->stack[$i][5]=$comb[2];
					$this->stack[$i][6]=$number[3];			
					$temp=$this->stack[$i];
					self::compare($temp);
					$i++;
				}
			}
			elseif (count($number)==5){
				$combs=self::combos($this->operators,4);
				foreach($combs as $comb){
					$this->stack[$i][0]=$number[0];
					$this->stack[$i][1]=$comb[0];
					$this->stack[$i][2]=$number[1];
					$this->stack[$i][3]=$comb[1];
					$this->stack[$i][4]=$number[2];
					$this->stack[$i][5]=$comb[2];
					$this->stack[$i][6]=$number[3];			
					$this->stack[$i][7]=$comb[3];
					$this->stack[$i][8]=$number[4];
					$temp=$this->stack[$i];
					self::compare($temp);
					$i++;
				}
			}		
		}
	}






	//function that returns all the combinations of operators
	private function combos($arr, $k) {
 	   if ($k==0) {
    	    return array(array());
    	}

    	if (count($arr) == 0) {
    	    return array();
    	}
		$head=$arr[0];
    	$combos=array();
    	$subcombos =self::combos($arr, $k-1);
    	foreach ($subcombos as $subcombo) {
        	array_unshift($subcombo, $head);
    	 	$combos[]=$subcombo;
    	}
    	array_shift($arr);
    	$combos=array_merge($combos,self::combos($arr, $k));
    	return $combos;
	}


	//function will compare the value of the expession and return the difference
	public function compare($string){
		$value=self::getValue($string);
		if (abs($this->target-$value)<$this->goal){
			$this->goal=$this->target-$value;
			$this->goalString=$string;
		}	
	}

	//function that gets the result of the string expression
	public function getValue ($string){
		for($i=1;$i<count($string);$i+=2){
			if ($string[$i]=='*' || $string[$i]=='/'){
				switch ($string[$i]){

					//if expression is multiplication
					case '*':
					$string[$i]=$string[$i-1]*$string[$i+1];
					array_splice($string,$i+1,1);
					array_splice($string,$i-1,1);
					$i-=2;
					break;

					//if expression is division
					case '/':
					if ($string[$i+1]>0){
						$string[$i]=$string[$i-1]/$string[$i+1];
						if (!is_float($string[$i])){
							array_splice($string,$i+1,1);
							array_splice($string,$i-1,1);
							$i-=2;
							break;
						}
						else{
							return 0;
						}
					}
					else{
						return 0;
					}
				}
			}
		}
		if(count($string)==1){
			$result=$string[0];
		}
		else{
			for($i=1;$i<count($string);$i+=2){
				$result=0;
				switch($string[$i]){

					//if expression is addition
					case '+':
					$string[$i]=$string[$i-1]+$string[$i+1];
					$result+=$string[$i];
					array_splice($string,$i+1,1);
					array_splice($string,$i-1,1);
					$i-=2;
					break;

					//if expression is substraction
					case '-':
					$string[$i]=$string[$i-1]-$string[$i+1];
					if ($string[$i]>=0){
						$result+=$string[$i];
						array_splice($string,$i+1,1);
						array_splice($string,$i-1,1);
						$i-=2;
						break;
					}
					else{
						return 0;

					}
				}
			}
		}
		return $result;
	}
	

}


?>