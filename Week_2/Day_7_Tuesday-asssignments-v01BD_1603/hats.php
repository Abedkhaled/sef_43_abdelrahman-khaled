<?php

//initializing input
$initialhatcolors = array();
$guessedcolors = array();
$blackstate = array();
echo "please enter size :";
$inputsize = fopen ("php://stdin","r");
$arraysize=fgets($inputsize);

for ($i=0;$i<$arraysize;$i++){
	$handle = fopen ("php://stdin","r");
	$input=fgets($handle);
	$initialhatcolors[$i]=trim($input ,"\n");
}

//last in row sees which color is odd
$blacks = countsblack($arraysize-1,$initialhatcolors);
//check if black is even or odd
if($blacks%2 != 0){
	$guessedcolors[$arraysize-1] = "black";
	$blackstate[$arraysize-1] = "odd";
	for ($i=$arraysize-2;$i>=0;$i--) {
		$blacks=countsblack($i,$initialhatcolors);
		//assign black flag
		if ($blacks%2 == 0){
			$blackstate[$i] = "even";
		}
		else {
			$blackstate[$i] = "odd";
		}
		//compare flag with previous flag and assume color
		if ($blackstate[$i]==$blackstate[$i+1]){
			$guessedcolors[$i] = "white";
		}
		else {
			$guessedcolors[$i] = "black";
		}
	}
}
else {
	$guessedcolors[$arraysize-1]="white";
	$blackstate[$arraysize-1]="even";
	for ($i=$arraysize-2;$i>=0;$i--) {
		$blacks = countsblack($i,$initialhatcolors);
		//assign black flag
		if ($blacks%2 == 0){
			$blackstate[$i] = "even";
		}
		else {
			$blackstate[$i] = "odd";
		}
		//compare flag with previous flag
		if ($blackstate[$i]==$blackstate[$i+1]){
			$guessedcolors[$i] = "white";
		}
		else {
			$guessedcolors[$i] = "black";
		}
	}
}
//printing the guessed solution
echo "--------------------------\n";
for ($j=0;$j<$arraysize;$j++){
	echo $guessedcolors[$j].("\n");
}

//checking if passes 
$result=array_diff($guessedcolors,$initialhatcolors);
if (count($result)<=1){
	echo ("succeeded\n");
}
//function that calculates the number of remaining blacks
function countsblack ($index,$hatcolors){
	$black=0;
	for ($j=0;$j<$index;$j++){
		if ($hatcolors[$j] == "black"){
			$black++;
		}
	}
	return $black;	
}
?>
