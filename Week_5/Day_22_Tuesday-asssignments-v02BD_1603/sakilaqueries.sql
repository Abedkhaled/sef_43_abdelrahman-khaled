exercice 1:

select actor_id,count(film_id) 
from 
	film_actor
group by 
	actor_id;

exercice 2:

select 
	language.name,
	count(language.language_id)
from 
	film
inner join 
	language
where 
	film.language_id=language.language_id
group by 
	language.name
order by language.name
limit 3;


exercice 3:

select 
	country.country,
	count(customer_id) as c1
from 
	customer
inner join 
	address,city,country
where
	customer.address_id=address.address_id
	and address.city_id=city.city_id
	and city.country_id=country.country_id
group by country.country
order by c1 desc
limit 3;

exercice 4:

select 
	address2 
from 
	address
where 
	address2 not like ''
	and address2 is not null
order by 
	address2;

exercice 5:
select 
	actor.first_name,
	actor.last_name,
	release_year
from
	actor
inner join 
	film_actor,
	film
where
	actor.actor_id=film_actor.actor_id
	and	film_actor.film_id=film.film_id

	and
		(description like '%crocodile%' 
		or description like '%shark%')
order by 
	last_name;

exercice 6:

select 
	SQL_CALC_FOUND_ROWS *
from
	(select 
		name,
		n.c1 as row
	from
		(select 
			name,
			count(film.film_id) as c1
		from 
			category
		inner join
			film_category,
			film
		where
			category.category_id=film_category.category_id
			and film_category.film_id=film.film_id
		group by name) as n
	where n.c1 > 85
		and n.c1 < 95
	order by n.c1 desc) as table1

union
select *
	from
		(select 
		name,
		n.c1 as row
	from
		(select 
			name,
			count(film.film_id) as c1
		from 
			category
		inner join
			film_category,
			film
		where
			category.category_id=film_category.category_id
			and film_category.film_id=film.film_id
		group by name) as n
	where n.c1 < 85
		or n.c1 > 95
	order by n.c1 desc) as table2
where found_rows() = 0
;

exercice 7:

select 
	p2.first_name,
	p2.last_name  
from
	actor as p1
inner join 
	actor as p2
where 
	p1.first_name=p2.first_name
	and p1.actor_id =8
	and p2.actor_id!=8
union
select 
	customer.first_name,
	customer.last_name
from
	customer
inner join 
	actor
where	
	customer.first_name=actor.first_name
	and actor.actor_id=8;

exercice 8:

select 
	t2.id2 as store,
	t2.year,
	t2.month,
	t2.totalmonth,
	t3.average
from
(select
	store.store_id as id2,
	year(rental_date) as year,
	month(rental_date) as month,
	sum(amount) as totalmonth
from 
	rental
inner join
	staff,
	store,
	payment
where 
	store.store_id=staff.store_id
	and staff.staff_id=rental.staff_id
	and rental.rental_id=payment.rental_id
group by id2, 
year(rental_date), 
month(rental_date)) as t2
inner join 
(
select
	t1.ret as id3,
	t1.year,
	t1.month,
	avg(t1.totalmonth) as average
from
(select
	store.store_id as ret,
	year(rental_date) as year,
	month(rental_date) as month,
	sum(amount) as totalmonth
from 
	rental
inner join
	staff,
	store,
	payment
where 
	store.store_id=staff.store_id
	and staff.staff_id=rental.staff_id
	and rental.rental_id=payment.rental_id
group by ret, 
year(rental_date), 
month(rental_date)) as t1
) as t3

group by t2.id2,t2.year,t2.month
;
exercice 9 :

select 
	customer.first_name,
	customer.last_name,
	count(rental_id) as c
from 
	rental 
inner join 
	customer
where
	customer.customer_id=rental.customer_id
group by customer.first_name,customer.last_name
order by c desc
limit 3;

