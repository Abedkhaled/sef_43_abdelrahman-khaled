use legal_claims;
select 
	minData.minId, Claims.patient_name,ClaimStatusCode.claim_status
	from
	(select 
		maximumData.id as minId, min(maximumData.number) as minNumber
	from
		(select 
			claim_id as id ,count(claim_sequence) as number
		from 
			LegalEvents
		inner join 
			ClaimStatusCode
		where 
			LegalEvents.claim_status=ClaimStatusCode.claim_status
		group by 
			defendant_name,claim_id) as maximumData
	group by 
		maximumData.id) as minData
inner join 
	Claims,ClaimStatusCode
where
	Claims.claim_id=minData.minId
	and
	ClaimStatusCode.claim_sequence=minData.minNumber;
