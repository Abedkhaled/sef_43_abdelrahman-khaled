<?php
require_once ("Config.php");
class MySQLWrap {

	private $dbCon;

	//function that creates a connection with the sakila database
	public function openConnection() {
		$credentials=new databaseCredentials();
		$this->dbCon=new mysqli ($credentials->getHostname(),$credentials->getUsername(),$credentials->getPassword(),$credentials->getDatabase());
		if ($this->dbCon->connect_error) {
			die($this->dbCon->connect_error);
		} else {
			echo "connected <br>";
		}
	}

	//function that closes the connection with the sakila database
	public function closeConnection() {
		$this->dbCon->close();
	}

	//function that executes the insert query on the rental 
	public function insertInRental($rental_id,$rental_date,$inventory_id,$customer_id,$staff_id) {
		$query="insert into rental values('$rental_id','$rental_date','$inventory_id','$customer_id',null,'$staff_id',null)";
		$this->dbCon->query($query);
		echo "rent success<br>";
		echo "rent ID :".$rental_id."<br>";
		echo "date of rent :".$rental_date;
	}

	//function that returns the inventory number of available books in available
	public function checkInventoryCount($film_name){
		$findIdquery="select film.film_id
					from rental 
					inner join
						film,inventory
					where
						film.film_id=inventory.film_id
						and rental.inventory_id=inventory.inventory_id
						and film.title='$film_name'
						group by (film.film_id)";
		$queryResult=$this->dbCon->query($findIdquery);
		if ($queryResult->num_rows > 0){
			$row=$queryResult->fetch_assoc();
			$id=$row["film_id"];
			$query="select *
				from
					(select 
						maximum.iid,
						maximum.return_date
					from
						(select 
							rental_id as rid, 
							inventory.inventory_id as iid, 
							return_date from rental
						inner join 
							inventory
						on 
							inventory.inventory_id=rental.inventory_id
						where 
							film_id='$id'
						order by 
							rental_id desc) as maximum
					group by maximum.iid) as sorted
				where return_date is not null";
			$queryResult=$this->dbCon->query($query);
 			if ($queryResult->num_rows >0){
 				$row=$queryResult->fetch_assoc();
 				return $row["iid"];
 			} else {
 				echo "no remaning copies in directory<br>";
 				return null;
 			}
		} else {
			echo "This film does not exist <br>";
			return null;
		}	
		
	}
	//function that gets the last rental id for incremental purposes
	public function getLastRentId (){
		$query="select rental_id from rental order by rental_id desc limit 1";
		$queryResult=$this->dbCon->query($query);
		$lastRent=$queryResult->fetch_assoc();
		return $lastRent["rental_id"]+1;
	}
}
?>
