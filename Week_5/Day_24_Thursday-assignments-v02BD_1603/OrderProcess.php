<?php
require_once("MySQLWrap.php");
$customer=$_POST['customer_id'];
$film=$_POST['film_name'];
$date=date('d-m-Y h:i:s');
$staff=$_POST['staff'];

//create a connection object
$connect=new MySQLWrap();
//start connection
$connect->openConnection();
//check if film exists
$inventory_number=$connect->checkInventoryCount($film);
if ($inventory_number!=null){
	//check the last rental id for incremental puroposes
	$rental_id=$connect->getLastRentId();

	//insert a rental method
	$connect->insertInRental($rental_id,$date,$inventory_number,$customer,$staff);
}

//end connection
$connect->closeConnection();

?>