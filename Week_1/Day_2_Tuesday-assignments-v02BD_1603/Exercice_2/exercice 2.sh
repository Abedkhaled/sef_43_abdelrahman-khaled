#!/bin/bash
limit=80
bool1=true
i=2
diskSpaceUsed=$(df | grep -v "Filesystem" | awk '{ print $5 }' | sed 's/%//g')
usedMemory=$(free | grep "Mem:" | awk '{print $3*100/$2}' )
intUsedMemory=${usedMemory%.*}

for percent in $diskSpaceUsed
do
	if [ $percent -gt $limit ]
	then
			name=$(df | head -$i | tail -1 | awk '{print $1}')
			echo "Alarm : Disk "$name" is "$percent"% full"
			bool1=false
	fi
	let i=$i+1
done



for percent1 in $intUsedMemory
do
	if [ $percent1 -gt $limit ]
	then 
		echo "Alarm : Memory is ${percent1}% full "
		bool1=false
	fi
done


if [ $bool1 == true ]
then	
	echo "Everything is good"
fi
