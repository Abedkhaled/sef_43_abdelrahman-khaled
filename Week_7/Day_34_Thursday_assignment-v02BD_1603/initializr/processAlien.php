<?php

//defining the keys to be used
define('APPLICATION_ID',   "4ad706c7");
define('APPLICATION_KEY',   "0bb1e421cb4889714bbd7a73993f633d");

function call_api($endpoint, $parameters) {
	//initialzing the curl
   $ch = curl_init('https://api.aylien.com/api/v1/' . $endpoint);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   curl_setopt($ch, CURLOPT_HTTPHEADER, array(
       'Accept: application/json',
       'X-AYLIEN-TextAPI-Application-Key: ' . APPLICATION_KEY,
       'X-AYLIEN-TextAPI-Application-ID: '. APPLICATION_ID
   ));
   curl_setopt($ch, CURLOPT_POST, true);
   curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
   $response = curl_exec($ch);
   return json_decode($response);
}

//getting the title and text as post
$tit=$_POST['title'];
$text=$_POST['paragraph'];
//calling the api function
$sum = call_api('summarize', array('title' => $tit,'text'=> $text, 'sentences_number' => 3));


//returning the summarized text to javascript
echo print(implode($sum->sentences)),
PHP_EOL;
curl_close($ch);

?>
