<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        
    </head>
    <body>
        <div id="big-container">
            <div id="input">
                <h1>Summarizor</h1>
                <div id="small-container">
                    <input type="text" name="in" id="inputURL">
                    <button id="but" onclick="summary.sendurl()">load page</button>
                </div>
                <div id="sum-head"></div>
                <div id="summarized"></div>
                <div id="container"></div>
            </div>
        </div>
        <script type="text/javascript">

            //function that sends an xml http request to the getHtml.php file
            function loadXMLDoc(sumary)
            {
                xmlhttp=new XMLHttpRequest();
                var url="sum="+sumary;
                xmlhttp.open("POST","getHtml.php",true);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.onreadystatechange=function()
                {
                    if (xmlhttp.readyState==4 && xmlhttp.status==200)
                    {
                       document.getElementById("container").innerHTML=xmlhttp.responseText;
                           summary.getText();
                    }
                }
               
                xmlhttp.send(url);
            }

            //function that sends an http request to the process alien php file
            function sendApi(tit,text) {
                //creating an xmlhttprequest
                xhr= new XMLHttpRequest();
                var data="title="+tit+"& paragraph="+text;
                xmlhttp.open("POST","processAlien.php",true);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.onreadystatechange=function()
                {
                    if (xmlhttp.readyState==4 && xmlhttp.status==200)
                    {
                        document.getElementById("sum-head").innerHTML=summary.title;
                       document.getElementById("summarized").innerHTML=xmlhttp.responseText;
                    }
                }
               
                xmlhttp.send(data);
            }


            function ValidURL(str) {
                var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
              if(!regexp.test(str)) {
                alert("Please enter a valid URL.");
                return false;
              } else {
                return true;
              }
            }

            //object summary initization
            var summary={
                //title of text
                title:"",
                //text to summarize
                orgText:"",
                //the url of the text to summarize
                url:"",
                
                //function that send url to the gethtml request
                sendurl:function(){
                    this.url=document.getElementById("inputURL").value;
                    var valid=ValidURL(this.url);
                    if (valid==true){
                        var load=document.createElement("div");
                        load.className="load-icon";
                        document.getElementById("summarized").appendChild(load);
                        loadXMLDoc(this.url);
                    }
                },
                
                //function that get the text out of the returned html
                getText:function(){
                    if (document.getElementById("3c01")!= null){
                        this.title=document.getElementsByClassName("graf--title")[0].innerHTML;
                    }
                    var par=document.getElementsByClassName("graf--p");
                    for(var i = 0; i < par.length; i++) {
                        this.orgText=this.orgText+par[i].innerHTML;
                    }
                    document.getElementById("container").innerHTML="";
                    this.sendTextToAlien();
                },

                //function that initialize the request function
                sendTextToAlien:function(){
                    sendApi(this.title,this.orgText);
                }
            }


        </script>
    </body>
</html>
