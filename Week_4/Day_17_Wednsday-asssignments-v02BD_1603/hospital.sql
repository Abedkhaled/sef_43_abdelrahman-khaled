create database HospitalRecords;
create table AnestProcedures (proc_id int primary key auto_increment, anest_name varchar(20), start_time time, end_time time);

create view view_procedures (proc_id,comparison_pros,anest_name,event_time,event_type)
as select p1.proc_id, p2.proc_id, p1.anest_name, p2.start_time, +1
from AnestProcedures as p1, AnestProcedures as p2
where p1.anest_name=p2.anest_name
and not (p2.end_time<=p1.start_time)
or p2.start_time>=p1.end_time
union
select p1.proc_id, p2.proc_id, p1.anest_name, p2.end_time, -1 as event_type
from AnestProcedures as p1 ,AnestProcedures as p2
where p1.anest_name=p2.anest_name
and not (p2.end_time<=p1.start_time)
or p2.start_time>=p1.end_time;


//to calculate the maximum overlap we shoud get the maximum count of event_type from the view table

select e1.proc_id, e1.event_time, 
(sum(e2.event_type)
from view_procedures as e2
where e2.proc_id=e1.proc_id
and e2.event_time<e1.event_time)
as instantaneous_count
from view_procedures as e1
order by e1.proc_id, e1.event_time;

//get result

select e1.proc_id
max(select sum(e2.event_type)
from view_procedures as e2
where e2.proc_id=e1.proc_id
and e2.event_time<e1.event_time)
as maximum_inst_count
from view_procedures as e1
group by e1.proc_id