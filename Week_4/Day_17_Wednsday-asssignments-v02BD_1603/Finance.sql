create database FinanceDB;
create table FiscalYearTable(fiscal_year YEAR(4), start_date date, end_date date);

delimiter $$
create trigger validate_date before insert on FiscalYearTable 
for each row 
begin 
if year(new.end_date) != year(new.start_date)+1
or year(new.start_date) != fiscal_year
or month (new.start_date) != 10 
or month (new.end_date) != 9 
or day (new.start_date) != 1 
or day (new.end_date) != 31 
then 
signal sqlstate '45000' 
set message_text = 'invalid date' ; 
end if; 
end;$$

